import {info, alertView, loading, loaderView, tableData, tableView, chartsView} from './views.js'

(function (
    window, document, info, alertView,
    loading, loaderView,
    tableData, tableView,
    chartsView
) {
    "use strict";

    if (!window.___INSTANCE_APP_DATA___) {
        alert('Please set api url to window.___INSTANCE_APP_DATA___!');
    }

    window.loading = loading;
    window.info = info;

    window.addEventListener('load', () => {
        loaderView.mount('#pageLineLoader', true);
        tableView.mount('#mainTableWrapper', false);
        alertView.mount('#pagePopupAlert', true);
        chartsView.mount('#mainChartsWrapper', false);
    });

})(window, document, info, alertView, loading, loaderView, tableData, tableView, chartsView);
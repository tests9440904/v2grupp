import {
    companyNameColumn,
    createChart, createRequest,
    createView,
    prepareDatasetForChart,
    prepareHeadersForChart,
    ref
} from "./base.js"


export const requestTableApi = createRequest(
    {
        loadstart: () => {
            loading.value = true;
            tableData.value = null;
        },
        loadend:  () => {
            loading.value = false;
        },
        load: function(){
            if (this.status !== 200) {
                info.value = {message: 'Ошибка ' + this.status + ': ' + this.statusText, type: 'error'};
            } else {
                tableData.value = this.response;
            }
        },
        error:  function(){
            info.value = {message: 'Ошибка соединения', type: 'error'};
            tableData.value = null;
        }
    }
);

export const info = ref({message: null, type: null});
let infoTimer = null;
export const alertView = createView(
    {
        data: {info},
        template: () => {
            if (infoTimer) {
                window.clearTimeout(infoTimer);
                infoTimer = null;
            }
            if (info.value.message || info.value.type) {
                const popup = document.createElement('div');
                popup.className = 'pagePopupAlert';
                if (info.value.type === 'error') {
                    popup.className += ' pagePopupAlertError';
                }

                const title = document.createElement('h6');
                title.innerText = info.value.type === 'error' ? 'Ошибка!' : 'Информация';
                title.className = 'pagePopupAlertTitle';

                const message = document.createElement('span');
                message.className = 'pagePopupAlertMessage';
                message.innerHTML = info.value.message;


                infoTimer = window.setTimeout(() => {
                    info.value = {message: null, type: null};
                }, 5000);

                popup.appendChild(title);
                popup.appendChild(message);
                return popup;
            }
            return null;
        }
    }
);


export const loading = ref(false);
export const loaderView = createView(
    {
        data: {loading, info},
        template: () => {
            const loader = document.createElement('div');
            loader.className = 'pageLineLoader';
            if (loading.value) {
                loader.className += ' pageLineLoaderAction';
            }
            if (info.value.type === 'error') {
                loader.className += ' pageLineLoaderError';
            }
            loader.id = 'pageLineLoader';

            if (loading.value) {
                const tracker = document.createElement('div');
                tracker.className = 'pageLineLoaderTrack';
                loader.appendChild(tracker);
            }

            return loader;
        }
    }
);


export const tableMenuVisible = ref(false);
const tableMenuItems = [
    {
        title: 'Обновить данные', 'onclick': () => {
            requestTableApi('GET', window.___INSTANCE_APP_DATA___.url);
            tableMenuVisible.value = false;
        }
    },
    {
        title: 'Использовать "рыбу"', 'onclick': () => {
            tableData.value = JSON.parse(JSON.stringify(def));
            tableMenuVisible.value = false;
        }
    },
    {
        title: 'Очистить', 'onclick': () => {
            tableData.value = null;
            tableMenuVisible.value = false;
        }
    }
];

export const tableHeaders = {
    'PREDPR_NAIM': 'предприятие',
    'REPEAT_ENT': 'коэф. повторяемости',
    'FACTOR_ELIMINATE': 'коэф. устраняемости',
    'KOL_NARUSH': 'кол-во нарушений',
    'KOL_PREDPIS': 'кол-во предписаний',
    'PRIOSTANOVKI': 'приостановки работ',
    'ISP_V_SROK': 'выполнены в срок',
    'ISP_NE_V_SROK': 'выполнены не в срок',
    'NE_ISTEK_SROK': 'на контроле',
    'ISTEK_SROK': 'просрочено',
    'VYP': 'выполнено'
}

export const def = [
    {
        "PREDPR_K": "1",
        "PREDPR_NAIM": "В2 Групп",
        "REPEAT_ENT": "0.66",
        "FACTOR_ELIMINATE": "0.74",
        "KOL_NARUSH": "194",
        "KOL_PREDPIS": "154",
        "PRIOSTANOVKI": "21",
        "ISP_V_SROK": "141",
        "ISP_NE_V_SROK": "2",
        "ISP_V_SROK_ISP_NE_V_SROK": "143",
        "IS_ISCONTRACTOR": "Y",
        "NE_ISTEK_SROK": "43",
        "ISTEK_SROK": "8",
        "NE_ISTEK_SROK_ISTEK_SROK": "51",
        "VYP": "143"
    },
    {
        "PREDPR_K": "3",
        "PREDPR_NAIM": "Обогатительная фабрика",
        "REPEAT_ENT": "0.5",
        "FACTOR_ELIMINATE": "0.69",
        "KOL_NARUSH": "171",
        "KOL_PREDPIS": "165",
        "PRIOSTANOVKI": "25",
        "ISP_V_SROK": "105",
        "ISP_NE_V_SROK": "31",
        "ISP_V_SROK_ISP_NE_V_SROK": "136",
        "IS_ISCONTRACTOR": "",
        "NE_ISTEK_SROK": "19",
        "ISTEK_SROK": "16",
        "NE_ISTEK_SROK_ISTEK_SROK": "35",
        "VYP": "136"
    },
    {
        "PREDPR_K": "2",
        "PREDPR_NAIM": "Разрез",
        "REPEAT_ENT": "0.34",
        "FACTOR_ELIMINATE": "0.4",
        "KOL_NARUSH": "179",
        "KOL_PREDPIS": "66",
        "PRIOSTANOVKI": "21",
        "ISP_V_SROK": "57",
        "ISP_NE_V_SROK": "21",
        "ISP_V_SROK_ISP_NE_V_SROK": "78",
        "IS_ISCONTRACTOR": "",
        "NE_ISTEK_SROK": "32",
        "ISTEK_SROK": "69",
        "NE_ISTEK_SROK_ISTEK_SROK": "101",
        "VYP": "78"
    },
    {
        "PREDPR_K": "14",
        "PREDPR_NAIM": "Талдинская зап.1",
        "REPEAT_ENT": "0.5",
        "FACTOR_ELIMINATE": "0.06",
        "KOL_NARUSH": "16",
        "KOL_PREDPIS": "13",
        "PRIOSTANOVKI": "5",
        "ISP_V_SROK": "1",
        "ISP_NE_V_SROK": "0",
        "ISP_V_SROK_ISP_NE_V_SROK": "1",
        "IS_ISCONTRACTOR": "",
        "NE_ISTEK_SROK": "15",
        "ISTEK_SROK": "0",
        "NE_ISTEK_SROK_ISTEK_SROK": "15",
        "VYP": "1"
    },
    {
        "PREDPR_K": "4",
        "PREDPR_NAIM": "Шахта",
        "REPEAT_ENT": "0.89",
        "FACTOR_ELIMINATE": "0.88",
        "KOL_NARUSH": "673",
        "KOL_PREDPIS": "663",
        "PRIOSTANOVKI": "208",
        "ISP_V_SROK": "559",
        "ISP_NE_V_SROK": "67",
        "ISP_V_SROK_ISP_NE_V_SROK": "626",
        "IS_ISCONTRACTOR": "",
        "NE_ISTEK_SROK": "21",
        "ISTEK_SROK": "26",
        "NE_ISTEK_SROK_ISTEK_SROK": "47",
        "VYP": "626"
    }
];
const preparedTableHeads = prepareHeadersForChart(tableHeaders, companyNameColumn);
export const tableData = ref([]);

export const tableView = createView(
    {
        data: {tableData, tableMenuVisible},
        template: () => {
            const table = document.createElement('table');
            table.setAttribute('cellpadding', '0');
            table.setAttribute('cellspacing', '0');

            const tHead = document.createElement('thead');
            let tr = document.createElement('tr');
            let td = null;
            let span = null;
            for (let column in tableHeaders) {
                if (tableHeaders[column]) {
                    td = document.createElement('th');
                    span = document.createElement('span');
                    span.className = 'mainTableColumnTitle';
                    span.innerHTML = tableHeaders[column];
                    td.appendChild(span);
                    tr.appendChild(td);
                }
            }
            tHead.appendChild(tr);

            const tBody = document.createElement('tbody');
            if (tableData.value) {
                for (let row of tableData.value) {
                    tr = document.createElement('tr');
                    for (let column in tableHeaders) {
                        if (row[column]) {
                            td = document.createElement('td');
                            td.innerHTML = row[column];
                            tr.appendChild(td);
                        }
                    }
                    tBody.appendChild(tr);
                }
            }

            table.appendChild(tHead);
            table.appendChild(tBody);


            const menu = tableMenuVisible.value
                ? document.createElement('ul')
                : document.createComment('toggle');
            if (tableMenuVisible.value) {
                menu.className = 'mainTableMenu';
                for (let item of tableMenuItems) {
                    let li = document.createElement('li');
                    li.className = 'mainTableMenuItem';
                    li.innerHTML = '<span>' + item.title + '</span>';
                    if (typeof item.onclick === 'function') {
                        li.addEventListener('click', item.onclick);
                    }
                    menu.appendChild(li);
                }
            }

            const menuButton = document.createElement('div');
            menuButton.innerHTML = '<span></span><span></span><span></span>';
            menuButton.className = 'mainTableMenuToggler';
            if (tableMenuVisible.value) {
                menuButton.className += ' mainTableMenuTogglerActive';
            }
            menuButton.addEventListener(
                'click',
                () => {
                    tableMenuVisible.value = !tableMenuVisible.value
                }
            )

            return [table, menuButton, menu];
        },
        onMounted: () => {
            window.setTimeout(() => {
                requestTableApi('GET', window.___INSTANCE_APP_DATA___.url);
            }, 1000)
        }
    }
);

let chart = null;
export const chartsView = createView(
    {
        data: {tableData, tableHeaders},
        template: () => {
            const canvas = document.createElement('canvas');
            canvas.id = 'mainCharts';
            chart = null;
            return canvas;
        },
        onMounted: () => {
            const newData = prepareDatasetForChart(tableData.value, tableHeaders, companyNameColumn);
            chart = createChart(document.getElementById('mainCharts'), newData, preparedTableHeads);
        },
        onUpdated: () => {
            const newData = prepareDatasetForChart(tableData.value, tableHeaders, companyNameColumn);
            if (!chart) {
                chart = createChart(document.getElementById('mainCharts'), newData, preparedTableHeads);
            } else {
                chart.data.datasets = newData;
                chart.data.labels = preparedTableHeads;
                chart.update();
            }
        }
    }
)

export const companyNameColumn = 'PREDPR_NAIM';

export const ref = (val) => {

    const object = {
        value: val,
        _watchers: []
    }

    return new Proxy(object, {
        set(target, prop, value) {
            if (prop.indexOf('_') === 0) {
                console.warn('try set private field: ' + prop);
                return true;
            }
            if (prop === 'value') {
                object[prop] = value;
                for (let watcher of object._watchers) {
                    watcher();
                }
                return true;
            }
        }
    });
}

export const createView = (description) => {
    let container = null;
    let replaced = false;

    const update = (isMount = false) => {
        try {
            let _container = description.template();

            if (!_container) {
                _container = document.createComment('node');
            }

            if (!Array.isArray(_container)) {
                _container = [_container];
            }

            if (replaced) {
                let idx = 0;
                for (let _cnt of _container) {
                    if (idx === 0) {
                        container.replaceWith(_cnt);
                        container = _cnt;
                    } else {
                        _cnt.insertAfter(_container[0]);
                    }
                    idx++;
                }
                return;
            }

            container.innerHTML = null;
            for (let _cnt of _container) {
                container.appendChild(_cnt);
            }
            if (!isMount && typeof description.onUpdated === 'function') {
                description.onUpdated({container});
            }
        } catch (e) {
            console.error(e);
        }
    }

    const mount = (id, isNeedReplace) => {
        try {
            replaced = isNeedReplace;
            container = document.getElementById(id.replace('#', ''));
            if (!container) {
                return null;
            }
            for (let variable in description.data) {
                if (description.data[variable] && description.data[variable]._watchers) {
                    description.data[variable]._watchers.push(update.bind(this))
                }
            }
            update(true);
            if (typeof description.onMounted === 'function') {
                description.onMounted({container});
            }
        } catch (e) {
            console.error(e);
        }
    }

    return {mount}
}

export const prepareDatasetForChart = (data, headers, columnForLabel) => {

    if (!data || !columnForLabel) {
        return [];
    }

    const datasets = {};

    for (let row of data) {
        datasets[row[columnForLabel]] = {
            label: row[columnForLabel],
            data: [],
            parsing: {
                xAxisKey: 'x',
                yAxisKey: 'y'
            }
        }

        if (headers) {
            for (let key in headers) {
                if (columnForLabel !== key) {
                    datasets[row[columnForLabel]].data.push({x: headers[key], y: row[key]});
                }
            }
        }
    }
    return Object.values(datasets);
}

export const prepareHeadersForChart = (headers, columnForLabel) => {
    const tmp = {... headers};
    delete tmp[columnForLabel];
    return Object.values(tmp);
}

export const createChart = (el, data, head) => {
    return new Chart(
        el,
        {
            type: 'bar',
            data: {
                labels: head,
                datasets: data
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                aspectRatio: '1|2',
                onResize: function() {},
                /*
                scales: {
                    x: {
                        stacked: true
                    },
                    y: {
                        stacked: true
                    }
                },
                */
                parsing: {
                    xAxisKey: companyNameColumn
                }
            }
        }
    );
}


export const createRequest = ({loadstart: loadstart, loadend: loadend, load: load, error: error}) => {

    const xhr = new XMLHttpRequest();

    if (typeof loadstart === 'function') {
        xhr.onloadstart = loadstart.bind(xhr);
    }
    if (typeof loadend === 'function') {
        xhr.onloadend = loadend.bind(xhr);
    }
    if (typeof load === 'function') {
        xhr.onload = load.bind(xhr);
    }
    if (typeof error === 'function') {
        xhr.onerror = error.bind(xhr);
    }

    return (method, url, body = null, headers = null) => {
        xhr.abort();

        xhr.open(method, url);
        xhr.timeout = 5000;
        xhr.responseType = 'json';
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.send(body)
    };
}